/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usb.gestion.mvc.dto;

import co.usb.gestion.common.util.Generales;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class RequisitoTipoInvestigadorDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String idTipoRequisito = Generales.EMPTYSTRING;
    String descripcionTipoRequisito = Generales.EMPTYSTRING;
    String idOpcion = Generales.EMPTYSTRING;
    String descripcionOpcion = Generales.EMPTYSTRING;
    String idsubTipoInvestigador = Generales.EMPTYSTRING;
    String descripcionSubTipoInvestigador = Generales.EMPTYSTRING;
    String codigoSubTipoInvestigador = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdTipoRequisito() {
        return idTipoRequisito;
    }

    public void setIdTipoRequisito(String idTipoRequisito) {
        this.idTipoRequisito = idTipoRequisito;
    }

    public String getDescripcionTipoRequisito() {
        return descripcionTipoRequisito;
    }

    public void setDescripcionTipoRequisito(String descripcionTipoRequisito) {
        this.descripcionTipoRequisito = descripcionTipoRequisito;
    }

    public String getIdOpcion() {
        return idOpcion;
    }

    public void setIdOpcion(String idOpcion) {
        this.idOpcion = idOpcion;
    }

    public String getDescripcionOpcion() {
        return descripcionOpcion;
    }

    public void setDescripcionOpcion(String descripcionOpcion) {
        this.descripcionOpcion = descripcionOpcion;
    }

    public String getIdsubTipoInvestigador() {
        return idsubTipoInvestigador;
    }

    public void setIdsubTipoInvestigador(String idsubTipoInvestigador) {
        this.idsubTipoInvestigador = idsubTipoInvestigador;
    }

    public String getDescripcionSubTipoInvestigador() {
        return descripcionSubTipoInvestigador;
    }

    public void setDescripcionSubTipoInvestigador(String descripcionSubTipoInvestigador) {
        this.descripcionSubTipoInvestigador = descripcionSubTipoInvestigador;
    }

    public String getCodigoSubTipoInvestigador() {
        return codigoSubTipoInvestigador;
    }

    public void setCodigoSubTipoInvestigador(String codigoSubTipoInvestigador) {
        this.codigoSubTipoInvestigador = codigoSubTipoInvestigador;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
