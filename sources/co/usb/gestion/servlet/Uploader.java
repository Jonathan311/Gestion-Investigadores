/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.servlet;

import co.usb.gestion.common.util.Generales;
import co.usb.gestion.mvc.dto.PerfilDTO;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Sys. Hebert Medelo
 */
@WebServlet(name = "Uploader", urlPatterns = {"/Uploader"})
public class Uploader extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            System.out.println((char) 27 + "[31;32m ruta fisica del archivo");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        HttpSession servletSesion = null;

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        try {

            if (ServletFileUpload.isMultipartContent(request)) {

                FileItemFactory itemfactory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(itemfactory);

                servletSesion = request.getSession(false);
                List<FileItem> items = upload.parseRequest(request);
                String dirImg = "";
                ServletContext srvcon = getServletContext();
                String rutafisica = srvcon.getRealPath("");
                System.out.println((char) 27 + "[31;43m ruta fisica del archivo" + rutafisica);

                String pathToWeb = Generales.EMPTYSTRING;
                if (System.getProperty("os.name").toLowerCase().startsWith("window")) {
                    pathToWeb = (String) new InitialContext().lookup("java:comp/env/ruta_imagen_perfil_windows");
                } else if (System.getProperty("os.name").toLowerCase().startsWith("linux")) {
                    pathToWeb = (String) new InitialContext().lookup("java:comp/env/ruta_imagen_perfil_linux");

                }
                PerfilDTO datos = null;
                datos = (PerfilDTO) servletSesion.getAttribute("datosUsuario");
                for (FileItem item : items) {

                    if (item.getName() != null) {
                        if ("image/jpeg".equals(item.getContentType()) || "image/png".equals(item.getContentType())) {
                            File uploadDir = new File(pathToWeb);
                            File file = new File(uploadDir, "profile" + datos.getId() + item.getName().substring(item.getName().lastIndexOf(".")));
                            item.write(file);
                            dirImg = file.getName();
                            System.out.println("subido con exito");
                            servletSesion.setAttribute("img", dirImg);

                        } else {
                            System.out.println((char) 27 + "[32;43m solo se reciben archivos png , jpeg  y jpg");
                            new Exception("tipo no valido");
                        }
                    }
                }

            }
            response.getWriter().write("hola");
        } catch (Exception ex) {
            Logger.getLogger(Uploader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
