/*
 * ContextDataResourceNames.java
 *
 * Proyecto: Gestion_Investigadores
 * Cliente: 
 * Copyright 2018 by Universidad Simon Bolivar Ext. Cucuta 
 * All rights reserved
 */
package co.usb.gestion.mvc.fachada;

import co.usb.gestion.mvc.dto.PerfilDTO;
import co.usb.gestion.mvc.mediador.MediadorSeguridad;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.directwebremoting.annotations.ScriptScope;

/**
 *
 * @author Sys. Hebert Medelo
 */
@RemoteProxy(name = "ajaxSeguridad", scope = ScriptScope.SESSION)
public class FachadaSeguridad {

    /**
     *
     */
    public FachadaSeguridad() {
    }

    /**
     *
     * @return
     */
    @RemoteMethod
    public boolean servicioActivo() {
        return true;
    }

    /**
     *
     * @param usuario
     * @return
     */
    @RemoteMethod
    public PerfilDTO consultarDatosUsuarioLogueado(String usuario) {
        return MediadorSeguridad.getInstancia().consultarDatosUsuarioLogueado(usuario);
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * LOS METODOS APARTIR DE AQUI NO HAN SIDO VALIDADOS
     * -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     */
}
